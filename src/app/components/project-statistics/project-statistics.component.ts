import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Project } from 'src/app/models/project.model';

@Component({
  selector: 'app-project-statistics',
  templateUrl: './project-statistics.component.html',
  styleUrls: ['./project-statistics.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectStatisticsComponent implements OnInit, OnChanges {
  @Input('projects') projects!: Project[];
  totalBudget!: number;
  currentActive!: number;

  constructor() { }

  ngOnInit(): void {
    this.setTotalBudget();
    this.setCurrentActive();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setTotalBudget();
    this.setCurrentActive();
  }

  setTotalBudget(): void {
    this.totalBudget = this.projects.map(x => x.budget).reduce((p, c) => {
      return p + c;
    }, 0)
  }

  setCurrentActive(): void {
    this.currentActive = this.projects.filter(x => x.status === 'working').length;
  }
}
