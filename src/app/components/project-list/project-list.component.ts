import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Observable, take } from 'rxjs';
import { Project } from 'src/app/models/project.model';
import { ProjectDataService } from 'src/app/services/project-data/project-data.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectListComponent implements OnInit {
  @Input('projects') projects: Project[] = [];

  constructor() {}

  ngOnInit(): void {
  }

}
