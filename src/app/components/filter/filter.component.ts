import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { map } from 'rxjs';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterComponent implements OnInit {
  @Input('title') title: string = '';
  @Input('values') values: string[] = [];
  @Input('control') control: FormControl = new FormControl('');
  @Output('valueSelected') valueSelected: EventEmitter<string> = new EventEmitter();
  filteredValues: string[] = this.values;
  showField: boolean = false;

  constructor(private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.control.valueChanges.pipe(
      map(value => this.filter(value))
    ).subscribe(values => {
      this.filteredValues = values;
    });
  }

  onSelect(event: MatAutocompleteSelectedEvent) {
    this.valueSelected.emit(event.option.value);
  }

  onChange(event: string) {
    if(event === '') {
      this.valueSelected.emit(event);
    }
  }

  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.values.filter(option => option.toLowerCase().includes(filterValue));
  }

}
