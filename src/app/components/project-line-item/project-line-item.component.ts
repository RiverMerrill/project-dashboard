import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FilterOptions } from 'src/app/models/filter-options.model';
import { Project } from 'src/app/models/project.model';

@Component({
  selector: 'app-project-line-item',
  templateUrl: './project-line-item.component.html',
  styleUrls: ['./project-line-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectLineItemComponent implements OnInit {
  @Input('project') project!: Project;
  editForm: FormGroup = new FormGroup({
    projectOwner: new FormControl(''),
    status: new FormControl(''),
    budget: new FormControl('')
  });
  editMode: boolean = false;
  statuses: string[] = ['new', 'working', 'archived', 'delivered']

  constructor(private snackBar: MatSnackBar, private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  toggleEditMode(): void {
    this.editMode = !this.editMode;
    if(this.editMode) {
      this.editForm.patchValue(this.project);
    } else {
      const undoData = this.project;
      this.project = {...this.project, ...this.editForm.value};
      this.showSnackbar(undoData);
    }
  }

  private showSnackbar(undoData: Project) {
      const snackBarRef = this.snackBar.open('Project Updated!', 'undo', {duration: 5000});
      snackBarRef.onAction().subscribe(() => {
        this.project = undoData;
        this.cd.detectChanges();
      });
  }
}
