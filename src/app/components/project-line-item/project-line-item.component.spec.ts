import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectLineItemComponent } from './project-line-item.component';

describe('ProjectLineItemComponent', () => {
  let component: ProjectLineItemComponent;
  let fixture: ComponentFixture<ProjectLineItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectLineItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectLineItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
