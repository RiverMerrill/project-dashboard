import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Filter } from 'src/app/models/filter.model';
import { FilterOptions } from 'src/app/models/filter-options.model';
import { FilterService } from 'src/app/services/filter/filter.service';

@Component({
  selector: 'app-project-filters',
  templateUrl: './project-filters.component.html',
  styleUrls: ['./project-filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectFiltersComponent implements OnInit {
  @Input('filterOptions') filterOptions!: FilterOptions;
  searchForm: FormGroup = new FormGroup({
    title: new FormControl(''),
    division: new FormControl(''),
    projectOwner: new FormControl(''),
    minBudget: new FormControl(''),
    maxBudget: new FormControl(''),
    status: new FormControl(''),
  });
  createdGroup: FormGroup = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });
  modifiedGroup: FormGroup = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });

  showBudget: boolean = false;
  showCreated: boolean = false;
  showModified: boolean = false;


  constructor(private filterService: FilterService) { }

  ngOnInit(): void {
  }

  filter(field: string, value: string): void {
    this.filterService.setFilter({field, value})
  }

  dateFilter(field: string, value: string): void {
    const isoString = new Date(value).toLocaleDateString('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' });
    this.filterService.setFilter({field, value: isoString});
  }

  getFormControl(formControlName: string): FormControl {
    return this.searchForm.get(formControlName) as FormControl;
  }
}
