import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Filter } from 'src/app/models/filter.model';
import { Filters } from 'src/app/models/filters.model';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  private readonly _filters = new BehaviorSubject<Filters>(new Filters());
  readonly filters = this._filters.asObservable();

  constructor() { }

  getFilters(): Observable<Filters> {
    return this.filters;
  }

  setFilter(filter: Filter): void  {
      this._filters.next({...this._getFilters(), [filter.field]: filter.value});
  }

  private _getFilters(): Filters {
    return this._filters.getValue();
  }
}
