import { KeyedWrite } from '@angular/compiler';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { take } from 'rxjs';
import { FilterOptions } from 'src/app/models/filter-options.model';
import { Filter } from 'src/app/models/filter.model';
import { Filters } from 'src/app/models/filters.model';
import { Project } from 'src/app/models/project.model';
import { FilterService } from 'src/app/services/filter/filter.service';
import { ProjectDataService } from 'src/app/services/project-data/project-data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
  projects: Project[] = []
  filteredProjects: Project[] = [];
  filterOptions!: FilterOptions;
  selectedFilters: Filter[] = []

  constructor(private projectDataService: ProjectDataService, private filterService: FilterService) { }

  ngOnInit(): void {
    this.projectDataService.getProjectData().pipe(take(1)).subscribe((projects) => {
      this.projects = projects;
      this.filteredProjects = projects;
      this.filterOptions = {
        titles: [...new Set(projects.map(project => project.title).sort())],
        divisions: [...new Set(projects.map(project => project.division).sort())],
        projectOwners: [...new Set(projects.map(project => project.projectOwner).sort())],
        budgets: [...new Set(projects.map(project => project.budget).sort())],
        statuses: [...new Set(projects.map(project => project.status).sort())],
        created: [...new Set(projects.map(project => project.created).sort())],
        modified: [...new Set(projects.map(project => project.modified).sort())]
      }
    });
    this.subscribeToFilters();
  }

  subscribeToFilters(): void {
    this.filterService.getFilters().subscribe(filters => {
      this.filteredProjects = this.projects.filter(project => {
        for (const key in filters) {
          const isMin = key.startsWith('min');
          const isMax = key.startsWith('max');
          const isRange = isMin || isMax;
          const projectKey = isRange ? key.substring(3).toLowerCase() as keyof Project : key as keyof Project;
          const filterKey = key as keyof Filters;
          if (
                (project[projectKey] === undefined ||
                project[projectKey] !== filters[filterKey]) &&
                filters[filterKey]
              ) {
              if(
                  (isMin && project[projectKey] < filters[filterKey]) ||
                  (!isMin && project[projectKey] > filters[filterKey]) ||
                  (!isMin && !isMax)
                ) {
                  return false;
              }
          }
        }
        return true;
      })
    });
  } 
}
