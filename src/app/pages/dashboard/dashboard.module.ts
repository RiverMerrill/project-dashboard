import { NgModule } from '@angular/core';

import { ProjectListComponent } from '../../components/project-list/project-list.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CommonModule } from '@angular/common';
import { ProjectLineItemComponent } from '../../components/project-line-item/project-line-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectFiltersComponent } from '../../components/project-filters/project-filters.component';
import { MatIconModule } from '@angular/material/icon';
import { FilterComponent } from 'src/app/components/filter/filter.component';
import { DashboardComponent } from './dashboard.component';
import { ProjectStatisticsComponent } from '../../components/project-statistics/project-statistics.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ProjectListComponent,
    ProjectLineItemComponent,
    ProjectFiltersComponent,
    FilterComponent,
    ProjectStatisticsComponent
  ],
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      MatInputModule,
      MatIconModule,
      MatSelectModule,
      MatCardModule,
      MatButtonModule,
      MatSnackBarModule,
      MatAutocompleteModule,
      MatDatepickerModule
  ],
  providers: [],
  exports: [DashboardComponent]
})
export class DashboardModule { }
