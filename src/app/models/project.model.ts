export interface Project {
    title: string;
    division: string;
    projectOwner: string;
    budget: number;
    status: string;
    created: string;
    modified: string;
}