export interface FilterOptions {
    titles: string[];
    divisions: string[],
    projectOwners: string[],
    budgets: number[],
    statuses: string[],
    created: string[],
    modified: string[]
}