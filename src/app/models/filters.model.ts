export class Filters {
    title: string;
    division: string;
    projectOwner: string;
    status: string;
    minBudget: number;
    maxBudget: number;
    minCreated: string;
    maxCreated: string;
    minModified: string;
    maxModified: string;

    constructor() {
        this.title = '';
        this.division = '';
        this.projectOwner = '';
        this.status = '';
        this.minBudget = 0;
        this.maxBudget = 0;
        this.minCreated = '';
        this.maxCreated = '';
        this.minModified = '';
        this.maxModified = '';
    }
}